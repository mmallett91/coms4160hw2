package edu.columbia.cs4160.pa1.life;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Tile {
	
	public float[] v0;
	public float[] v1;
	public float[] v2;
	
	private float[] centroid;
	
	private boolean alive;
	private boolean prevAlive;
	private boolean nextAlive;
	
	private static final float[] GROUND_COLOR = {0.3098039215686275f, 0.192156862745098f, 0.1098039215686275f};
	private static final float[] GROUND_MATERIAL = {1f,1f,1f,1f};//{0.3098039215686275f, 0.192156862745098f, 0.1098039215686275f, 1.0f};
	private FloatBuffer groundMaterial;
	
	private static final float[] TREE_COLOR = {0.1333f, 0.5450980392156863f, 0.1333f};
	private static final float[] TREE_MATERIAL = {.3f,.3f,.3f,1f};//{0.1333f, 0.5450980392156863f, 0.1333f, 1.0f};
	private FloatBuffer treeMaterial;
	private float percent;
	
	private static Texture dirt;
	private static Texture tree;
	
	private int displayListIndex;

	public Tile(float[] v0, float[] v1, float[] v2){
		this(v0, v1, v2, false);
	}
	
	public Tile(float[] v0, float[] v1, float[] v2, boolean alive){
		this.v0 = v0;
		this.v1 = v1;
		this.v2 = v2;
		this.alive = alive;
		this.nextAlive = false;
		this.prevAlive = false;
		this.percent = 0f;
		
		groundMaterial = BufferUtils.createFloatBuffer(GROUND_MATERIAL.length);
		groundMaterial.put(GROUND_MATERIAL);
		groundMaterial.flip();
		
		treeMaterial = BufferUtils.createFloatBuffer(TREE_MATERIAL.length);
		treeMaterial.put(TREE_MATERIAL);
		treeMaterial.flip();
		
		centroid = centroid(v0, v1, v2);
		
		initDisplayList();
		
	}
	
	private void initDisplayList(){
		
		displayListIndex = GL11.glGenLists(1);
		
		GL11.glNewList(displayListIndex, GL11.GL_COMPILE);
		
			GL11.glBegin(GL11.GL_TRIANGLES);
				GL11.glColor3f(GROUND_COLOR[0], GROUND_COLOR[1], GROUND_COLOR[2]);
				GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT, groundMaterial);
				GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_DIFFUSE, groundMaterial);
				GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SPECULAR, groundMaterial);
				//float[] centroid = centroid(v0, v1, v2);
				GL11.glNormal3f(centroid[0], centroid[1], centroid[2]);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3f(v0[0], v0[1], v0[2]);
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex3f(v1[0], v1[1], v1[2]);
				GL11.glTexCoord2f(1, 1);
				GL11.glVertex3f(v2[0], v2[1], v2[2]);
			GL11.glEnd();
			
		GL11.glEndList();
		
	}
	
	/**
	 * loads textures
	 */
	public static void init(){
		
		try {
			dirt = TextureLoader.getTexture("jpg",  ResourceLoader.getResourceAsStream("resources/dirt.jpg"));
			tree = TextureLoader.getTexture("jpg", ResourceLoader.getResourceAsStream("resources/grass.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * draw this tile
	 * if the tile is alive, draw the tree at its correct height %
	 */
	public void draw(){
		
		tree.bind();
		
		GL11.glCallList(displayListIndex);
		
		drawTree();
	}

	/**
	 * used during world construction
	 * split this tile into 4 subtiles
	 * repeatedly called to divide 8 triangles into a high-res sphere
	 * @return
	 */
	public List<Tile> subdivide(){
		float[] mid02 = normalize(midpoint(v0, v2));
		float[] mid01 = normalize(midpoint(v0, v1));
		float[] mid12 = normalize(midpoint(v1, v2));
		
		List<Tile> ret = new ArrayList<Tile>();
		ret.add(new Tile(v0, mid01, mid02));
		ret.add(new Tile(mid01, v1, mid12));
		ret.add(new Tile(mid02, mid12, v2));
		ret.add(new Tile(mid01, mid12, mid02));
		
		return ret;
	}
	
	public void setAlive(boolean alive){
		prevAlive = false;
		this.alive = alive;
	}
	
	public boolean isAlive(){
		return alive;
	}
	
	/**
	 * simulate conway's game of life for this tile
	 * iterates neighbors, counts how many are alive
	 * count will dictate if the tile survives the generation
	 * @param neighbors
	 */
	public void evolve(Set<Tile> neighbors){
		
		int count = 0;
		
		for(Tile t : neighbors){
			if(!t.equals(this)){
				count += (t.alive) ? 1 : 0;
			}
		}
		
		// using B1/S2 rules of conway's game of life
		if(!alive){
			nextAlive = count == 1;// || count == 3;
		}
		else{
			nextAlive = count == 2;// || count == 4;// || count == 6;
		}
		
	}
	
	/**
	 * move the tile to the next generation
	 */
	public void nextGen(){
		prevAlive = alive;
		alive = nextAlive;
	}
	
	private float[] midpoint(float[] v0, float[] v1){
		float[] ret = new float[3];
		for(int i=0; i<3; i++){
			ret[i] = (v0[i] + v1[i])/2f;
		}
		return ret;
	}

	private float[] normalize(float[] v){
		float[] ret = new float[3];
		float mag = (float) Math.sqrt((v[0]*v[0])+(v[1]*v[1])+(v[2]*v[2]));
		for(int i=0; i<3; i++){
			ret[i] = v[i] / mag;
		}
		return ret;
	}

	private float[] centroid(float[] v0, float[] v1, float[] v2){
		float[] ret = new float[3];
		for(int i=0; i<3; i++){
			ret[i] = (v0[i] + v1[i] + v2[i])/3.0f;
		}
		return ret;
	}
	
	/*private float[] calcNormal(float[] v0, float[] v1, float[] v2){
		
		float[] u = new float[3];
		float[] v = new float[3];
		
		for(int i=0; i<3; i++){
			u[i] = v1[i] - v0[i];
			v[i] = v2[i] - v0[i];
		}
		
		
		float[] ret = new float[3];
		
		ret[0] = u[2] * v[3] - u[3] * v[2];
		ret[1] = u[3] * v[1] - u[1] * v[3];
		ret[2] = u[1] * v[2] - u[2] * v[1];
		
		return ret;
	}*/

	/**
	 * draws a tree on the tile if it is alive
	 * uses the percent member to scale the tree up or down
	 * if it is currently spawning or dying
	 */
	private void drawTree(){
		//float[] centroid = centroid(v0, v1, v2);
		float[] tip = new float[3];
		float tipScalar = 1.2f;
		
		if(!prevAlive && alive){ //just spawned
			tipScalar = 1f + (percent * .2f);
		}
		
		else if(prevAlive && !alive){ //just died
			tipScalar = 1.2f - (percent * .2f);
		}
		
		for(int i=0; i<3; i++){
			tip[i] = centroid[i] * tipScalar;
		}
		
		if(alive || prevAlive){
			
			tree.bind();
			
			GL11.glBegin(GL11.GL_TRIANGLE_FAN);
				// 34/255 139/255 34/255
				GL11.glColor3f(TREE_COLOR[0], TREE_COLOR[1], TREE_COLOR[2]); //a nice forest green
				// so soothing
				// many tree
				// wow
				GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT, treeMaterial);
				GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_DIFFUSE, treeMaterial);
				GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SPECULAR, treeMaterial);
				GL11.glNormal3f(centroid[0],centroid[1], centroid[2]);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex3f(tip[0], tip[1], tip[2]);
				GL11.glTexCoord2f(1,0);
				GL11.glVertex3f(v0[0], v0[1], v0[2]);
				GL11.glTexCoord2f(1,1);
				GL11.glVertex3f(v1[0], v1[1], v1[2]);
				GL11.glTexCoord2f(0,1);
				GL11.glVertex3f(v2[0], v2[1], v2[2]);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex3f(v0[0], v0[1], v0[2]);
			GL11.glEnd();
			
		}

	}

	/**
	 * sets the percent completed of the current generation
	 * used to animate tree spawn/death
	 * @param percent
	 */
	public void setPercent(float percent) {
		
		this.percent = percent;
		
	}

}

