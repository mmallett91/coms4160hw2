package edu.columbia.cs4160.pa1.life;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class LifeApplication {

    String windowTitle = "Life";
    private boolean closeRequested = false;

    long lastFrameTime; // used to calculate delta
    private World world;
    
    private float zoom;
    private static final float ZOOM_MAX = -2f;
    private static final float ZOOM_MIN = -7f;
    
    private long genStart;
    private long lastUpdate;
    
    private float percentGenComplete;
    private float rotation;
    
    private long genLength;
    
    private static final long GEN_MAX_MS = 10000;
    private static final long GEN_MIN_MS = 50;
    
    private static final float ROTATION_PER_SEC = .025f;
    
    public LifeApplication(){
    	
    	lastUpdate = genStart = System.currentTimeMillis();
    	
    	percentGenComplete = 0f;
    	rotation = 0f;
    	
    	zoom = -4f;
    	genLength = 1000;
    }

    public void run() {

        createWindow();
        initGL();
        
        world = new World();
        
        while (!closeRequested) {
            pollInput();
            
            updateLogic();
            renderGL();

            Display.update();
        }
        
        cleanup();
    }
    
    private void initGL() {

        /* OpenGL */
        int width = Display.getDisplayMode().getWidth();
        int height = Display.getDisplayMode().getHeight();

        GL11.glViewport(0, 0, width, height); // Reset The Current Viewport
        GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        GL11.glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
        GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        GL11.glLoadIdentity(); // Reset The Modelview Matrix

        GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
        GL11.glClearDepth(1.0f); // Depth Buffer Setup
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
        GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
        
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        
        initLight();
        
        // these load textures
        Space.init();
        Tile.init();
        
        Camera.create();        
    }
    
    private void initLight(){
    	
    	float lightAmbient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
        float lightDiffuse[] = { 0.7f, 0.7f, 0.7f, 1.0f };
        float lightPosition[] = { 4.0f, 2.0f, 4.0f, 0.0f };
     
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_AMBIENT, arrayToBuffer(lightAmbient));              // Setup The Ambient Light
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_DIFFUSE, arrayToBuffer(lightDiffuse));              // Setup The Diffuse Light         
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_POSITION, arrayToBuffer(lightPosition));  
     
	    GL11.glEnable(GL11.GL_LIGHT1); 
	    GL11.glEnable ( GL11.GL_LIGHTING ) ;
    	
    }
    
    
    private FloatBuffer arrayToBuffer(float[] arr){
    	
    	FloatBuffer ret = BufferUtils.createFloatBuffer(arr.length);
    	
    	ret.put(arr);
    	ret.flip();
    	
    	return ret;
    	
    }
    
    private void updateLogic() {
//        triangleAngle += 0.1f * delta; // Increase The Rotation Variable For The Triangles
//        quadAngle -= 0.05f * delta; // Decrease The Rotation Variable For The Quads
    	long cur = System.currentTimeMillis();
    	
    	if(cur - genStart > genLength){
    		world.simulateGeneration();
    		genStart = System.currentTimeMillis();
    	}
    	
    	percentGenComplete = ((cur - genStart) / (float) genLength);
    	
    	// leaving the given getDelta() in only to keep the mouse rotations working
    	long delta = cur - lastUpdate;
    	
    	/*
    	 *  rotpersec * 1000 ms 
    	 * 
    	 */
    	
    	// rot/sec * 1sec/1000ms * 360deg/1 rot = x deg / ms, then multiply by number of ms
    	float rotateDeg = delta * ((ROTATION_PER_SEC / 1000f) * 360f);
    	
    	rotation += rotateDeg;
    	
    	if(rotation > 360f){
    		rotation -= 360f;
    	}
    	
    	lastUpdate = cur;
    }


    private void renderGL() {

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL11.glLoadIdentity(); // Reset The View
        GL11.glTranslatef(0.0f, 0.0f, zoom); // Move Right And Into The Screen
        
        Space.draw();

        Camera.apply();
        
        world.draw(percentGenComplete, rotation);
        
    }
        
        
    
    /**
     * Poll Input
     */
    public void pollInput() {
    	//i am using only the mouse rotations from Camera
        Camera.acceptInput(getDelta());
        // scroll through key events
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE){
                    closeRequested = true;
                }
                else if (Keyboard.getEventKey() == Keyboard.KEY_P){
                    snapshot();
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_W){ //zoom in
                	zoom = Math.min(zoom + 1f, ZOOM_MAX);
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_S){ //zoom out
                	zoom = Math.max(zoom - 1f, ZOOM_MIN);
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_A){ //slow life
                	genLength = Math.min(genLength * 2, GEN_MAX_MS);
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_D){ //speed life
                	genLength = Math.max(genLength / 2, GEN_MIN_MS);
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_R){ //reset
                	world = new World();
                }
                
            }
        }

        if (Display.isCloseRequested()) {
            closeRequested = true;
        }
    }
    
    

    public void snapshot() {
        System.out.println("Taking a snapshot ... snapshot.png");

        GL11.glReadBuffer(GL11.GL_FRONT);

        int width = Display.getDisplayMode().getWidth();
        int height= Display.getDisplayMode().getHeight();
        int bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
        ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
        GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );

        File file = new File("snapshot.png"); // The file to save to.
        String format = "PNG"; // Example: "PNG" or "JPG"
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
   
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                int i = (x + (width * y)) * bpp;
                int r = buffer.get(i) & 0xFF;
                int g = buffer.get(i + 1) & 0xFF;
                int b = buffer.get(i + 2) & 0xFF;
                image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
            }
        }
           
        try {
            ImageIO.write(image, format, file);
        } catch (IOException e) { e.printStackTrace(); }
    }
    
    /** 
     * Calculate how many milliseconds have passed 
     * since last frame.
     * 
     * @return milliseconds passed since last frame 
     */
    public int getDelta() {
        long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
        int delta = (int) (time - lastFrameTime);
        lastFrameTime = time;
     
        return delta;
    }

    private void createWindow() {
        try {
            Display.setDisplayMode(new DisplayMode(960, 540));
            Display.setVSyncEnabled(true);
            Display.setTitle(windowTitle);
            Display.create();
        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }
    
    /**
     * Destroy and clean up resources
     */
    private void cleanup() {
        Display.destroy();
    }
    
    public static void main(String[] args) {
        new LifeApplication().run();
    }
      
}
