package edu.columbia.cs4160.pa1.life;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;

import org.lwjgl.opengl.GL11;

public class World {
	
	private static final int PERCENT_ALIVE = 2;
	
	private ArrayList<Tile> tiles;
	private NeighborMap neighbors;
	
	public World(){
		this.tiles = new ArrayList<Tile>();
		this.neighbors = new NeighborMap();
		init();
	}
	
	/**
	 * generates a new world filled with tiles
	 * the tiles are randomly assigned alive or barren based on PERCENT_ALIVE
	 * maps neighboring tiles as tiles that share a vertex
	 */
	public void init(){
		// vertices of a somethingahedron
		float[][][] v = {
			{{0.0f, 1.0f, 0.0f}, {0.0f, -1.0f, 0.0f}},
			{{1.0f, 0.0f, 0.0f}, {-1.0f, 0.0f, 0.0f}},
			{{0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, -1.0f}}
		};
		
		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++){
				for(int k=0; k<2; k++){
					
					Tile t = new Tile(v[0][i], v[1][j], v[2][k]);
					for(Tile t2 : t.subdivide()){
						for(Tile t3 : t2.subdivide()){
							for(Tile t4 : t3.subdivide()){
								for(Tile t5 : t4.subdivide()){
									for(Tile t6 : t5.subdivide()){
										tiles.add(t6);
										// tiles are neighbors if they share a vertex
										// build map of neighbors
										neighbors.put(t6.v0, t6);
										neighbors.put(t6.v1, t6);
										neighbors.put(t6.v2, t6);
									}
								}
							}
						}
					}
					
				}
			}
		}
		
		

		Random r = new Random(System.currentTimeMillis());
		
		for(Tile t : tiles){
			t.setAlive(r.nextInt(100) < PERCENT_ALIVE);
		}
		
	}
	
	/**
	 * apply rotation of the world
	 * and then iterate all tiles, drawing each
	 * @param percent
	 * @param angle
	 */
	public void draw(float percent, float angle){
		
		GL11.glRotatef(angle, 0f, 1f, 0f);
		
		for(Tile t : tiles){
			t.setPercent(percent);
			t.draw();
		}
		
		
		
	}
	
	/**
	 * simulate a generation of life
	 * each tile has built in rules to simulate its life
	 * construct the neighboring tile list then simulate life for each tile
	 */
	public void simulateGeneration(){
		
		// first evolve them
		for(Tile t : tiles){
			HashSet<Tile> neighborSet = new HashSet<Tile>();
			neighborSet.addAll(neighbors.get(t.v0));
			neighborSet.addAll(neighbors.get(t.v1));
			neighborSet.addAll(neighbors.get(t.v2));
			t.evolve(neighborSet);
		}
		
		// then move to next gen
		for(Tile t : tiles){
			t.nextGen();
		}
		
	}
	
}
