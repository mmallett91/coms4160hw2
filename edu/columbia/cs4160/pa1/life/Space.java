package edu.columbia.cs4160.pa1.life;

import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Space {
	
	
	// use "slick util" jar for textures
	// http://slick.ninjacave.com/slick-util/
	private static Texture texture;
	
	private static FloatBuffer spaceMat;
	
	private static int index;
	
	/**
	 * loads textures
	 */
	public static void init(){
		
		try {
			texture = TextureLoader.getTexture("jpg",  ResourceLoader.getResourceAsStream("resources/space.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		float[] spaceMatArr = new float[]{1,1,1,1};
		
		spaceMat = BufferUtils.createFloatBuffer(spaceMatArr.length);
		spaceMat.put(spaceMatArr);
		spaceMat.flip();
		
		index = GL11.glGenLists(1);
		
		GL11.glNewList(index, GL11.GL_COMPILE);
		
			GL11.glBegin(GL11.GL_QUADS);
			
			GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT, spaceMat);
			GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_DIFFUSE, spaceMat);
			GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SPECULAR, spaceMat);
			GL11.glNormal3f(0, 0, 1);
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex3f(-6f, -6f, -1f);
			GL11.glTexCoord2f(1, 0);
			GL11.glVertex3f(10f, -6f, -1f);
			GL11.glTexCoord2f(1, 1);
			GL11.glVertex3f(10f, 11f, -1f);
			GL11.glTexCoord2f(0, 1);
			GL11.glVertex3f(-6f, 11f, -1f);
		
			GL11.glEnd();
		
		GL11.glEndList();
		
	}
	
	/**
	 * draws the space background
	 */
	public static void draw(){

		Color.white.bind();
		
		texture.bind();
		
		GL11.glCallList(index);
		
//		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);

		
		
	}

}
