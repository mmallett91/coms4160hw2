package edu.columbia.cs4160.pa1.life;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

public class NeighborMap {
	
	// a power of ten to shift decimal
	private static final float DECIMAL_SHIFT = 10000;
	
	private HashMap<String, HashSet<Tile>> neighbors;
	
	
	
	public NeighborMap(){
		
		neighbors = new HashMap<String, HashSet<Tile>>();
		
	}
	
	public void put(float[] v, Tile t){
		
		String key = buildKey(v);
		
		if(!neighbors.containsKey(key)){
			neighbors.put(key, new HashSet<Tile>());
		}
		
		HashSet<Tile> set = neighbors.get(key);
		
		set.add(t);
		
	}
	
	public HashSet<Tile> get(float[] v){
		
		String key = buildKey(v);
		
		return neighbors.get(key);
		
	}
	
	public String buildKey(float[] v){
		
		// make sure an equivalent vertex
		// gets put into the same hash bucket,
		// represent them as a string
		StringBuilder b = new StringBuilder();
		for(float f : v){
			int i = (int) (f * DECIMAL_SHIFT);
			b.append(i + "|");
		}
		
		return b.toString();
	}
	
	public Set<Entry<String, HashSet<Tile>>> entrySet(){
		
		return neighbors.entrySet();
		
	}
	
	
	

}
