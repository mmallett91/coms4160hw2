package edu.columbia.cs4160.pa1.gltests;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class World {

    String windowTitle = "3D Shapes";
    public boolean closeRequested = false;

    long lastFrameTime; // used to calculate delta
    
    float triangleAngle; // Angle of rotation for the triangles
    float quadAngle; // Angle of rotation for the quads

    public void run() {

        createWindow();
        getDelta(); // Initialise delta timer
        initGL();
        
        while (!closeRequested) {
            pollInput();
            updateLogic(getDelta());
            renderGL();

            Display.update();
        }
        
        cleanup();
    }
    
    private void initGL() {

        /* OpenGL */
        int width = Display.getDisplayMode().getWidth();
        int height = Display.getDisplayMode().getHeight();

        GL11.glViewport(0, 0, width, height); // Reset The Current Viewport
        GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        GL11.glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
        GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        GL11.glLoadIdentity(); // Reset The Modelview Matrix

        GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
        GL11.glClearDepth(1.0f); // Depth Buffer Setup
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
        GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
        Camera.create();        
    }
    
    private void updateLogic(int delta) {
        triangleAngle += 0.1f * delta; // Increase The Rotation Variable For The Triangles
        quadAngle -= 0.05f * delta; // Decrease The Rotation Variable For The Quads
    }


    private void renderGL() {

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL11.glLoadIdentity(); // Reset The View
        GL11.glTranslatef(0.0f, 0.0f, -7.0f); // Move Right And Into The Screen

        Camera.apply();
        
//        GL11.glBegin(GL11.GL_TRIANGLE_FAN);
//        GL11.glColor3f(0.0f, 1.0f, 0.0f);
//        GL11.glVertex3f(0, 1.0f, 0);
//        GL11.glVertex3f(1.0f, 0.0f, 0.0f);
//        GL11.glVertex3f(0.0f, 0.0f, 1.0f);
//        GL11.glVertex3f(-1.0f, 0.0f, 0.0f);
//        GL11.glVertex3f(0.0f, 0.0f, -1.0f);
//        GL11.glVertex3f(1.0f, 0.0f, 0.0f);
//        GL11.glEnd();
//        
//        GL11.glBegin(GL11.GL_TRIANGLE_FAN);
//        GL11.glColor3f(0.0f, 1.0f, 0.0f);
//        GL11.glVertex3f(0, -1.0f, 0);
//        GL11.glVertex3f(1.0f, 0.0f, 0.0f);
//        GL11.glVertex3f(0.0f, 0.0f, 1.0f);
//        GL11.glVertex3f(-1.0f, 0.0f, 0.0f);
//        GL11.glVertex3f(0.0f, 0.0f, -1.0f);
//        GL11.glVertex3f(1.0f, 0.0f, 0.0f);
//        GL11.glEnd();
//        
//        final float X = .525731112119133606f; 
//        final float Z = .850650808352039932f;
//         
//        float[][] vdata = {    
//           {-X, 0.0f, Z}, {X, 0.0f, Z}, {-X, 0.0f, -Z}, {X, 0.0f, -Z},    
//           {0.0f, Z, X}, {0.0f, Z, -X}, {0.0f, -Z, X}, {0.0f, -Z, -X},    
//           {Z, X, 0.0f}, {-Z, X, 0.0f}, {Z, -X, 0.0f}, {-Z, -X, 0.0f} 
//        };
//        
//        int[][] tindices = { 
//           {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},    
//           {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},    
//           {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6}, 
//           {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11} };
//         
//        GL11.glBegin(GL11.GL_TRIANGLES);  
//        GL11.glColor3f(1.0f, 0.0f, 0.0f);
//        for (int i = 0; i < 20; i++) {    
//           GL11.glColor3f(1.0f, 0.0f, 0.7f); 
//           GL11.glVertex3f(vdata[tindices[i][0]][0], vdata[tindices[i][0]][1], vdata[tindices[i][0]][2]); 
//           GL11.glVertex3f(vdata[tindices[i][1]][0], vdata[tindices[i][1]][1], vdata[tindices[i][1]][2]); 
//           GL11.glVertex3f(vdata[tindices[i][2]][0], vdata[tindices[i][2]][1], vdata[tindices[i][2]][2]); 
//        }
//        GL11.glEnd();
        
        float[] v0 = {0.0f, 1.0f, 0.0f};
        float[] v1 = {1.0f, 0.0f, 0.0f};
        float[] v2 = {0.0f, 0.0f, 1.0f};
        
        Triangle tri = new Triangle(v0, v1, v2);
//        tri.draw();
        
        for(Triangle t : tri.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
    				drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v3 = {0.0f, -1.0f, 0.0f};
        float[] v4 = {1.0f, 0.0f, 0.0f};
        float[] v5 = {0.0f, 0.0f, 1.0f};
        
        Triangle tri2 = new Triangle(v3, v4, v5);
//        tri.draw();
        
        for(Triangle t : tri2.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v6 = {0.0f, -1.0f, 0.0f};
        float[] v7 = {-1.0f, 0.0f, 0.0f};
        float[] v8 = {0.0f, 0.0f, 1.0f};
        
        Triangle tri3 = new Triangle(v6, v7, v8);
//        tri.draw();
        
        for(Triangle t : tri3.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v9 = {0.0f, -1.0f, 0.0f};
        float[] v10 = {-1.0f, 0.0f, 0.0f};
        float[] v11 = {0.0f, 0.0f, -1.0f};
        
        Triangle tri4 = new Triangle(v9, v10, v11);
//        tri.draw();
        
        for(Triangle t : tri4.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v12 = {0.0f, 1.0f, 0.0f};
        float[] v13 = {-1.0f, 0.0f, 0.0f};
        float[] v14 = {0.0f, 0.0f, -1.0f};
        
        Triangle tri5 = new Triangle(v12, v13, v14);
//        tri.draw();
        
        for(Triangle t : tri5.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v15 = {0.0f, 1.0f, 0.0f};
        float[] v16 = {1.0f, 0.0f, 0.0f};
        float[] v17 = {0.0f, 0.0f, -1.0f};
        
        Triangle tri6 = new Triangle(v15, v16, v17);
//        tri.draw();
        
        for(Triangle t : tri6.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v18 = {0.0f, 1.0f, 0.0f};
        float[] v19 = {-1.0f, 0.0f, 0.0f};
        float[] v20 = {0.0f, 0.0f, 1.0f};
        
        Triangle tri7 = new Triangle(v18, v19, v20);
//        tri.draw();
        
        for(Triangle t : tri7.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        float[] v21 = {0.0f, -1.0f, 0.0f};
        float[] v22 = {1.0f, 0.0f, 0.0f};
        float[] v23 = {0.0f, 0.0f, -1.0f};
        
        Triangle tri8 = new Triangle(v21, v22, v23);
//        tri.draw();
        
        for(Triangle t : tri8.subdivide()){
        	for(Triangle supat : t.subdivide()){
        		for(Triangle killdaetherT : supat.subdivide()){
        			killdaetherT.draw();
        			drawTree(killdaetherT);
        		}
        	}
        }
        
        
    }
    
    public class Triangle{
    	
    	public float[] v0;
    	public float[] v1;
    	public float[] v2;
    	
    	public Triangle(float[] v0, float[] v1, float[] v2){
    		this.v0 = v0;
    		this.v1 = v1;
    		this.v2 = v2;
    	}
    	
    	public void draw(){
    		GL11.glBegin(GL11.GL_TRIANGLES);
	    		GL11.glColor3f(0.7f, 0.0f, 1.0f);
	    		GL11.glVertex3f(v0[0], v0[1], v0[2]);
	    		GL11.glVertex3f(v1[0], v1[1], v1[2]);
	    		GL11.glVertex3f(v2[0], v2[1], v2[2]);
    		GL11.glEnd();
    	}
    	
    	public List<Triangle> subdivide(){
    		float[] mid02 = normalize(midpoint(v0, v2));
    		float[] mid01 = normalize(midpoint(v0, v1));
    		float[] mid12 = normalize(midpoint(v1, v2));
    		
    		List<Triangle> ret = new ArrayList<Triangle>();
    		ret.add(new Triangle(v0, mid01, mid02));
    		ret.add(new Triangle(mid01, v1, mid12));
    		ret.add(new Triangle(mid02, mid12, v2));
    		ret.add(new Triangle(mid01, mid12, mid02));
    		
    		return ret;
    	}
    }
    
    public float[] midpoint(float[] v0, float[] v1){
    	float[] ret = new float[3];
    	for(int i=0; i<3; i++){
    		ret[i] = (v0[i] + v1[i])/2f;
    	}
    	return ret;
    }
    
    public float[] normalize(float[] v){
    	float[] ret = new float[3];
    	float mag = (float) Math.sqrt((v[0]*v[0])+(v[1]*v[1])+(v[2]*v[2]));
    	for(int i=0; i<3; i++){
    		ret[i] = v[i] / mag;
    	}
    	return ret;
//    	return v;
    }
    
    public float[] centroid(float[] v0, float[] v1, float[] v2){
    	float[] ret = new float[3];
    	for(int i=0; i<3; i++){
    		ret[i] = (v0[i] + v1[i] + v2[i])/3.0f;
    	}
    	return ret;
    }
    
    public void drawTree(Triangle t){
    	float[] centroid = centroid(t.v0, t.v1, t.v2);
    	float tipScalar = 1.4f, baseScalar = 0.9f;
    	for(int i=0; i<3; i++){
    		centroid[i] *= tipScalar;
    	}
    	GL11.glBegin(GL11.GL_TRIANGLE_FAN);
    		// 34/255 139/255 34/255
    		GL11.glColor3f(0.1333f, 0.5450980392156863f, 0.1333f); //a nice forest green
    		// so soothing
    		// many tree
    		// wow
    		GL11.glVertex3f(centroid[0], centroid[1], centroid[2]);
    		GL11.glVertex3f(baseScalar * t.v0[0], t.v0[1], t.v0[2]);
    		GL11.glVertex3f(t.v1[0],baseScalar * t.v1[1], t.v1[2]);
    		GL11.glVertex3f(t.v2[0], t.v2[1], t.v2[2]);
    		GL11.glVertex3f(t.v0[0], t.v0[1],baseScalar * t.v0[2]);
    	GL11.glEnd();
    	
    }
    
    /**
     * Poll Input
     */
    public void pollInput() {
        Camera.acceptInput(getDelta());
        // scroll through key events
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
                    closeRequested = true;
                else if (Keyboard.getEventKey() == Keyboard.KEY_P)
                    snapshot();
            }
        }

        if (Display.isCloseRequested()) {
            closeRequested = true;
        }
    }

    public void snapshot() {
        System.out.println("Taking a snapshot ... snapshot.png");

        GL11.glReadBuffer(GL11.GL_FRONT);

        int width = Display.getDisplayMode().getWidth();
        int height= Display.getDisplayMode().getHeight();
        int bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
        ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
        GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );

        File file = new File("snapshot.png"); // The file to save to.
        String format = "PNG"; // Example: "PNG" or "JPG"
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
   
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                int i = (x + (width * y)) * bpp;
                int r = buffer.get(i) & 0xFF;
                int g = buffer.get(i + 1) & 0xFF;
                int b = buffer.get(i + 2) & 0xFF;
                image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
            }
        }
           
        try {
            ImageIO.write(image, format, file);
        } catch (IOException e) { e.printStackTrace(); }
    }
    
    /** 
     * Calculate how many milliseconds have passed 
     * since last frame.
     * 
     * @return milliseconds passed since last frame 
     */
    public int getDelta() {
        long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
        int delta = (int) (time - lastFrameTime);
        lastFrameTime = time;
     
        return delta;
    }

    private void createWindow() {
        try {
            Display.setDisplayMode(new DisplayMode(640, 480));
            Display.setVSyncEnabled(true);
            Display.setTitle(windowTitle);
            Display.create();
        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }
    
    /**
     * Destroy and clean up resources
     */
    private void cleanup() {
        Display.destroy();
    }
    
    public static void main(String[] args) {
        new World().run();
    }
      
}
